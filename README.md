## Set Hostname

This role sets hostname on machines.

Tested on debian 10.

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|-----------------------------------------|
| | | | | |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/set_hostname.git
  scm: git
  version: master
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: set_hostname
```

## Tests

not testable in docker
